from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'metascrum24.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += patterns('scrum.views',
    url(r'board/$', 'board', name='board'),

    url(r'^sprint/(?P<sprint_id>[0-9]+)/$',             'sprint',       name='sprint'),
    url(r'^sprint/add/$',                               'sprint_add',   name='sprint_add'),
    url(r'^sprint/(?P<sprint_id>[0-9]+)/update/$',      'sprint_update', name='sprint_update'),
    url(r'^sprint/(?P<sprint_id>[0-9]+)/burndown/$',    'sprint_burndown', name='sprint_burndown'),

    url(r'task/add/(?P<task_id>\d+)/$',                 'add_task',     name='ajax_add_task'),
    url(r'task/delete/(?P<task_id>\d+)/$',              'delete_task',  name='ajax_delete_task'),
    url(r'task/edit/(?P<task_id>\d+)/$',                'edit_task',    name='ajax_edit_task'),
)

