# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import handy.models.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Sprint',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(default=None, max_length=100, blank=True)),
                ('storypoints', models.PositiveSmallIntegerField(default=0, max_length=2, blank=True)),
                ('days', handy.models.fields.StringArrayField(max_length=30)),
                ('start', models.DateField()),
                ('finish', models.DateField(null=True, blank=True)),
                ('closed', models.DateField(null=True, blank=True)),
            ],
            options={
                'ordering': ('-id',),
                'get_latest_by': 'id',
                'verbose_name': 'sprint',
                'verbose_name_plural': 'sprints',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(default=None, max_length=300, blank=True)),
                ('description', models.TextField(default=None, blank=True)),
                ('notes', models.TextField(default=None, blank=True)),
                ('cost', models.PositiveSmallIntegerField(default=0, max_length=2, blank=True)),
                ('priority', models.PositiveSmallIntegerField(default=0, max_length=2, blank=True)),
                ('created', models.DateField(auto_now_add=True)),
                ('is_complete', models.BooleanField(default=False)),
                ('is_deleted', models.BooleanField(default=False)),
            ],
            options={
                'ordering': ('-id',),
                'verbose_name': 'task',
                'verbose_name_plural': 'tasks',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TaskStatus',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timestamp', models.DateTimeField()),
                ('cost', models.PositiveSmallIntegerField(default=0, max_length=2, blank=True)),
                ('status', models.PositiveSmallIntegerField(default=0, max_length=1, blank=True, choices=[(0, b'in planes'), (1, b'in process'), (2, b'code review'), (3, b'done')])),
                ('order', models.PositiveSmallIntegerField(default=0, blank=True)),
                ('type', models.PositiveSmallIntegerField(default=0, max_length=1, blank=True, choices=[(0, b'usual'), (1, b'moar'), (2, b'hot')])),
                ('sprint', models.ForeignKey(to='scrum.Sprint')),
                ('task', models.ForeignKey(to='scrum.Task')),
            ],
            options={
                'verbose_name': 'task status',
                'verbose_name_plural': 'task statuses',
            },
            bases=(models.Model,),
        ),
    ]
